# CRM API #

luiscrm.localhost

### What is this repository for? ###

* Converted users management.

### How do I get set up? ###
* Requirements
	1. LAMP or XAMPP server.
	2. Apache/2.4.27 or greater.
	3. PHP/7.0.22 or greater.

* Configuration
	1. Create `.env` file from `.env.*`.
	2. Adjust your new `.env` file.

* Database configuration
	Import DB from `database/Updates/*.sql` file.

* Dependencies
	Install Dependencies using `composer install`.

* Deployment instructions
	1. Backup `storage/app` dir.
	2. Backup `.env` file.
	3. Backup database.
	4. Copy content from`storage/app` to `storage/app.essential`.
	5. Follow Laravel documentation.

	* Agregar el siguiente comando en el archivo .env para cargar automaticamente la documentacion de swagger
	L5_SWAGGER_GENERATE_ALWAYS = true


### Contribution guidelines & documentation ###

+ API Features:

	* Show Link by AP (Looks for links.ap, thats the main ecosystem's one).
	* Show Link by User's ID (Looks for users.uuid).
	- (Find & Update) Or Store Link with Persona and Contacts
		1. Find or Create Contacts
		2. Find Persona by Contacts or Create
		3. Find Persona by user_id
		4. Update status_id, persona_id, contacts, user_id
		5. Find Link or Create
	* Create & Update User.
	* Create Listing
	* Show Server Info


* Database Key Fields:

	1. `links.ap`: uuid for links
 	2. `links.owner_id`: user.id, The user wich has this converted user.
 	3. `listings.owner_id`: user.id, The users wich created and owns the listing, but other can use them.
 	4. `link_user`: Pivot, Relation if exists, between users and converted users (links). Ex. When user converts while register account or when who converts is a registered user.

* Workflow: [Gitflow](http://nvie.com/posts/a-successful-git-branching-model/).
* Framework: [Laravel 5.5](https://laravel.com/docs/5.5).
* Admin: [Voyager 1.0](https://voyager.readme.io/docs).

***
2020 [Lum Labs](https://luiscrm.localhost)
