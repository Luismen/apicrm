<?php

namespace App\LooseAnnotations;

     /**
     *
     * @OA\RequestBody(
     *     request="step",
     *     description="User object that needs to be added to CRM",
     *     required=true,
     *     @OA\JsonContent(ref="#/components/schemas/Step"),
     *     @OA\Schema(ref="#/components/schemas/Step")
     * )
     */
