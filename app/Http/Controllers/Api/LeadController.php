<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Lead;

class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      /*  $leads =Lead::All();
        return $leads;*/
        return Lead::paginate();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $leads = Lead::create($request->all());
      return $leads;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $leads = Lead::where("id",$id)->firstOrFail();

      return [
          "message" => "Resource Found.",
          "data" => $leads
      ];

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $message = [];

      $leads = Lead::where("id",$id)->firstOrFail();

      if (isset($request->name)){
          $leads->name = $request->name;
          $message[] = "Updated: ".$leads->name." to ".$request->name;
      }

      if (isset($request->email)){
          $leads->email = $request->email;
          $message[] = "Updated: ".$leads->email." to ".$request->email;
      }

      if (isset($request->phone)){
          $leads->phone = $request->phone;
          $message[] = "Updated: ".$leads->phone." to ".$request->phone;
      }

      if (isset($request->message)){
          $leads->message = $request->message;
          $message[] = "Updated: ".$leads->message." to ".$request->message;
      }

      if (isset($request->step_id)){
          $leads->step_id = $request->step_id;
          $message[] = "Updated: ".$leads->step_id." to ".$request->step_id;
      }

      $leads->save();

      return [
          "message" => $message,
          "dataset" => $leads
      ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $leads = Lead::where("id",$id)->firstOrFail();

      $leads->delete();
      return [
          "message" => "Resource Deleted",
          "dataset" => $leads
      ];
    }
}
