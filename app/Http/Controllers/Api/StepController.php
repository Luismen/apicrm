<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Step;

/**
* @OA\Info(title="API ", version="1.0")
*
* @OA\Server(url="http://voyager.dev.localhost")
*/
class StepController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
      $steps =Step::All();
      return $steps;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $steps = Step::create($request->all());
        return $steps;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function show($id)
    {
      $steps = Step::where("id",$id)->firstOrFail();

      return [
          "message" => "Resource Found.",
          "data" => $steps
      ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $message = [];

      $steps = Step::where("id",$id)->firstOrFail();

      if (isset($request->key)){
          $steps->key = $request->key;
          $message[] = "Updated: ".$steps->key." to ".$request->key;
      }

      if (isset($request->value)){
          $steps->value = $request->value;
          $message[] = "Updated: ".$steps->value." to ".$request->value;
      }

      $steps->save();

      return [
          "message" => $message,
          "dataset" => $steps
      ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $steps = Step::where("id",$id)->firstOrFail();

      $steps->delete();
      return [
          "message" => "Resource Deleted",
          "dataset" => $steps
      ];
    }
}
